# Problems
A list of problems I found with the original `app.py`:
* **FIXED** Login details are stored in plaintext.
* **FIXED** Login details are stored in a dict.
* **FIXED** Passwords are not checked.
* **FIXED** No way to log out.
* **FIXED** You can post/view messages without being logged in.
* **FIXED** Any user can post as any other user.
* **FIXED** There are SQL injection attack vectors.
* **FIXED** No way to register new accounts.
* **Partial fix** Almost everything is in one file.
* **Not fixed** There are CSRF attack vectors.
* **FIXED** There are XSS attack vectors.

# Features
* Logout path has been added.
* Passwords are salted and hashed, then stored in an SQL database.
* New accounts can be created.
* New messages use the current user's username as the sender name.
* A very crude private messaging system.

# Running
To demo the app install flask and run the command `python3 -m flask run`.
It will open a webserver on `localhost:5000`.
The main pages are:
* `localhost:5000` Main page where you can send public messages.
* `localhost:5000/private_messages` Where you can send and receive private messages.
* `localhost:5000/login` Login page.
* `localhost:5000/logout` Logout page.
* `localhost:5000/register` Page where you can register new users.

### Using the website
When you first connect to `localhost:5000`, you will be redirected to the login page.
Currently, there are no users, so you have to go to the `localhost:5000/register` page to
make a user. On the login page there is a button that says `Register`. Press it, and it
will redirect you to the registration page.

After you register, you will be redirected back to the login page, where you have to
log in with your newly created account. After logging in you will see the main public
message board. Here you can send a message, search for messages with text or show all
messages that have been sent so far.

If you now go to `localhost:5000/private_messages` you will see a slightly different page.
Here you can search for specific messages by id, or send a message to someone. The "Send to"
field is where you choose who you are sending to. It is a comma seperated list of
recipients. You can either specify ids or usernames, but NO SPACES unless the username
itself contains spaces. Here are some examples of recipient lists:
* `1,2,3,4` Sends to users with id 1, 2, 3 and 4.
* `bob,alice` Sends to users "bob" and "alice".
* `1,3,alice` Sends to users with id 1 and 3, and also to "alice".

The "subject" field is for what the message is about. Pretty simple. Lastly, the "Message"
field is just for the main text of the message.

# Technical details
The logout function is pretty easy, just add a route which calls `flask_login.logout_user()`
and then redirects to `/`.

The SQL database for the users is also simple, only storing the username,
password+salt hash, and the salt. On account creation passwords are combined with a
randomly generated uuid5 as the salt, and then they are hashed using sha512.

I often get the currently logged-in user, and that is done by using flask's
`flask_login.current_user.get_id()` to get the user id, and then using that to look in
the database to retrieve the rest of the user data (mostly username and the separate
table for private messages).

Speaking of private messages, that was implemented by, on user creation, making a new table
based on the new user's id. I created a new API according to the assignment:

* ``POST /new`` to create a new message. Returns JSON with success or error as status,
and a reason for any errors that may happen.
* ``GET /messages/ID`` to retrieve a single message in JSON format.
* ``GET /messages`` to retrieve (a list of) all messages in a JSON list. Each element
is the same format as the individual ``GET /messages/ID``s.

Additionally:
* ``GET /useridinfo?id=USERID`` Gets the username for id, because ``GET /messages``
only gives the sending user's id, not the name itself.

Using this, I wrote some javascript to make the messages look prettier, and put it in
``private_messages.html``. This file is what is sent if you access
`localhost:5000/private_messages`.

# Questions
* [Threat model](https://cheatsheetseries.owasp.org/cheatsheets/Threat_Modeling_Cheat_Sheet.html)
– who might attack the application? What can an attacker do? What damage could be done (in terms of confidentiality, integrity, availability)? Are there limits to what an attacker can do? Are there limits to what we can sensibly protect against?
  * This app is for sending messages either publicly or privately. An attacker would probably want to
see what is being sent between private parties. Currently, there is still a CSRF vulnerability
that allows attackers to potentially trick people into sending or giving away their private
messages. Other than that there is most likely other XSS attack vectors that I have not
found, but no SQL injections I believe. An attacker could probably DDOS the server.
* What are the main attack vectors for the application?
  * The main attack vectors are probably the private messages. I'm not sure if I
have protected the usernames when it comes to XSS, and in the private messages the names
are displayed. I did find a function for sanitizing untrusted strings in javascript, which
uses regex to replace special characters with safe HTML representations of them. I'm not
really sure if it covers all special characters.
* What should we do (or what have you done) to protect against attacks?
  * Sanitize all strings going in and out of the browser to protect against XSS.
  * Make sure all user controlled text in SQL queries are sanitized.
  * Use CSRF tokens to prevent against CSRF. I have not done this one.
* What is the [access control model](https://cheatsheetseries.owasp.org/cheatsheets/Authorization_Cheat_Sheet.html)?
  * Currently, if you are not logged in you can't really access anything. I would say
that I'm using deny by default.
* How can you know that you security is good enough? ([traceability](https://cheatsheetseries.owasp.org/cheatsheets/Logging_Cheat_Sheet.html))
  * You really can never truly know, but logging every action the server is doing and every
interaction other computers have with the server is a good start. Then you can at least
see what is going on to hopefully take preventative measures if you see any suspicious
activity.