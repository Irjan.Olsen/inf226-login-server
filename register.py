from app import app, conn, get_user

import flask
from flask import request, render_template

import hashlib
import uuid

from register_form import RegisterForm


@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegisterForm()
    if form.is_submitted():
        print(f'Received form: {"invalid" if not form.validate() else "valid"} {form.form_errors} {form.errors}')
        print(request.form)
    if form.validate_on_submit():
        username = form.username.data
        password = form.password.data
        c = conn.execute("SELECT * FROM users WHERE username GLOB ?", (username,))
        row = c.fetchone()

        # We don't want to create multiple users with the same username!
        if row:
            flask.abort(409) # 409 Conflict

        salt = str(uuid.uuid4())
        hasher = hashlib.sha512()
        hasher.update(password.encode())
        hasher.update(salt.encode())
        password_hash = hasher.hexdigest()

        #users[username] = {"hash": password_hash, "salt": salt}
        conn.execute("INSERT INTO users (username, password_hash, salt) values (?, ?, ?);", (username, password_hash, salt))

        u = get_user(username)

        # Create a personal message table for each user. The recipients field is a comma seperated list of user id's.
        c.execute(f'''CREATE TABLE IF NOT EXISTS user{u.get("id")}_private_messages (
            id integer PRIMARY KEY, 
            sender INT NOT NULL,
            subject TEXT NOT NULL,
            recipients TEXT NOT NULL,
            message TEXT NOT NULL);''')

        return flask.redirect('/')
    return render_template('./register.html', form=form)