import login

from app import app, conn, User, get_user

from flask import request, send_from_directory

from json import dumps
from apsw import Error

from login import get_current_user, login_required

@app.get('/private_messages')
def priv_message():
    return send_from_directory(app.root_path,
                        'private_messages.html', mimetype='text/html')

@app.post('/new')
@login_required
def new_private_message():
    curr_user = get_current_user()
    recipients = request.args.get("recipients")
    recipients = ','.join(list(dict.fromkeys(recipients.split(","))))
    subject = request.args.get("subject")
    message = request.args.get("message")

    if not recipients or not subject or not message:
        return "{\"type\": \"error\", \"message\": \"Missing recipients, subject or message\"}"

    real_recip = ""

    for recipient in recipients.split(","):
        try:
            recip_id = int(recipient)
            u = User(recip_id)
            if u.id == 0:
                continue
        except:
            us = get_user(recipient)
            print(recipient, us)
            if not us:
                continue
            recip_id = us.get("id")
        if len(real_recip) > 0:
            real_recip += ","
        real_recip += str(recip_id)

    recipients = real_recip

    for recipient in recipients.split(","):
        try:
            recip_id = int(recipient)
            u = User(recip_id)
        except:
            us = get_user(recipient)
            print(recipient, us)
            if not us:
                continue
            u = User(us.get("id"))

        if not u or u.id == 0 or u.id == curr_user.id:
            continue
        conn.execute(
            f"INSERT INTO {u.private_messages_table} (sender, subject, recipients, message) values (?, ?, ?, ?);",
            (curr_user.id, recipients, subject, message)
        )

    conn.execute(
        f"INSERT INTO {curr_user.private_messages_table} (sender, subject, recipients, message) values (?, ?, ?, ?);",
        (curr_user.id, recipients, subject, message)
    )

    return "{\"type\": \"success\"}", 200

@app.get('/messages')
@login_required
def get_messages():
    user = get_current_user()
    c = conn.execute(f"SELECT * FROM {user.private_messages_table}")
    rows = c.fetchall()
    result = dumps(rows)
    c.close()
    return result

@app.get('/messages/<int:msg_id>')
@login_required
def get_msg_id(msg_id):
    user = get_current_user()
    c = conn.execute(f"SELECT * FROM {user.private_messages_table} WHERE id = ?", (msg_id,))
    row = c.fetchone()
    if row and len(row) > 0:
        return dumps(row)
    else:
        return "", 404