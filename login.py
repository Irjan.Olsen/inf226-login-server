from app import app, get_user, user_loader

import flask
from flask import request, render_template

import flask_login
from flask_login import login_user, login_required

import hashlib

from login_form import LoginForm

def check_password(password, salt, pass_hash):
    hasher = hashlib.sha512()
    hasher.update(password.encode())
    hasher.update(salt.encode())
    computed_hash = hasher.hexdigest()
    return computed_hash == pass_hash

def get_current_user():
    id = flask_login.current_user.get_id()
    if id:
        user = user_loader(id)
    else:
        user = {"id": 0, "username": "Anonymous"}
    return user

@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.is_submitted():
        print(f'Received form: {"invalid" if not form.validate() else "valid"} {form.form_errors} {form.errors}')
        print(request.form)
    if form.validate_on_submit():
        # TODO: we must check the username and password
        username = form.username.data
        password = form.password.data
        u = get_user(username)

        if u and check_password(password, u.get("salt"), u.get("hash")):
            user = user_loader(u.get("id"))

            # automatically sets logged in session cookie
            login_user(user)

            flask.flash('Logged in successfully.')

            next = flask.request.args.get('next')

            # is_safe_url should check if the url is safe for redirects.
            # See http://flask.pocoo.org/snippets/62/ for an example.
            if False and not is_safe_url(next):
                return flask.abort(400)

            return flask.redirect(next or flask.url_for('index'))
    return render_template('./login.html', form=form)


@app.route("/logout")
@login_required
def logout():
    flask_login.logout_user()
    return flask.redirect("/")